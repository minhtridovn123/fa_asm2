# Branch Managing Application - FA ASM 2
## Configuration
Change the connection string in appsettings.json
```
"ConnectionStrings": {
    "ApplicationDbContextConnection": "..."
  }
```
Add Migration
```
Add-Migration Init
```
Update database
```
Update-Database
```
