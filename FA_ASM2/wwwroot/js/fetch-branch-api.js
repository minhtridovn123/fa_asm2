﻿document.addEventListener("DOMContentLoaded",
    async function () {

        var name  = document.getElementById
            ("name");
        var address = document.getElementById
            ("address");
        var city = document.getElementById
            ("city");
        var state = document.getElementById
            ("state");
        var zipcode = document.getElementById
            ("zipcode");

        var insert = document.getElementById("insert");
        var update = document.getElementById("update");
        var del = document.getElementById("delete");

        var message = document.getElementById("message");

        message.innerHTML = "";


        const request = new Request("/Branches/SelectAllApi");


        const options = {
            method: "GET"
        };

        const response = await fetch(request, options);

        if (!response.ok) {
            message.innerHTML = `<h2>${response.status} 
            - ${response.statusText}</h2>`;
            return;
        }
        const json = await response.json();

        json.forEach(function (branch) {
            const body = document.getElementById("body");
            body.innerHTML += "<tr>"
                + `<td>${branch.name}</td>`
                + `<td>${branch.address}</td>`
                + `<td>${branch.city}</td>`
                + `<td>${branch.state}</td>`
                + `<td>${branch.zipCode}</td>`
                + "</tr> ";
            console.log(branch);
        });

        message.innerHTML = "Customers fetched successfully.";


        insert.addEventListener('click', async function () {

            const request = new Request("/Branches/InsertApi");

            let branch = {};
            branch.Name = name.value;
            branch.Address = address.value;
            branch.City = city.value;
            branch.State = state.value;
            branch.ZipCode = zipcode.value;

            console.log(branch);

            const token = document.getElementsByName
                ("__RequestVerificationToken")[0];

            const options = {
                method: "POST",
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'MY-XSRF-TOKEN': token.value,
                },
                body: JSON.stringify(branch)
            };
            const response = await fetch(request, options);

            if (!response.ok) {
                message.innerHTML = `<h2>${response.status} 
            - ${response.statusText}</h2>`;
                return;
            }
            const msg = await response.json();
            message.innerHTML = msg;
            location.reload();
        });
    });

