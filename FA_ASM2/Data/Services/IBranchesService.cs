﻿using FA_ASM2.Models;

namespace FA_ASM2.Data.Services
{
    public interface IBranchesService
    {
        List<Branch> GetAll();
        Branch Get(int id);
        void Add(Branch branch);
        void Update(Branch branch);
        void Delete(int id);
    }
}
