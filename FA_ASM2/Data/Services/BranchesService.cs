﻿using FA_ASM2.Models;

namespace FA_ASM2.Data.Services
{
    public class BranchesService : IBranchesService
    {
        private readonly AppDbContext _context;

        public BranchesService(AppDbContext context)
        {
            _context = context;
        }


        public void Add(Branch branch)
        {
            _context.Branches.Add(branch);
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            _context.Branches.Remove(Get(id));
            _context.SaveChanges();
        }

        public Branch Get(int id)
        {
            return _context.Branches.Find(id);
        }

        public List<Branch> GetAll()
        {
            return _context.Branches.ToList();
        }

        public void Update(Branch branch)
        {
            _context.Branches.Update(branch);
            _context.SaveChanges();
        }
    }
}
