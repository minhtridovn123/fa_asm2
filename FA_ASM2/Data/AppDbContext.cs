﻿using FA_ASM2.Models;
using Microsoft.EntityFrameworkCore;

namespace FA_ASM2.Data
{
    public class AppDbContext: DbContext
    {
        public DbSet<Branch> Branches { get; set; }
        public AppDbContext(DbContextOptions<AppDbContext> options): base(options)
        {

        }
    }
}
