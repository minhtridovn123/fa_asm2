﻿using FA_ASM2.Models;

namespace FA_ASM2.Data
{
    public class SeedDb
    {
        public static void Seed(IApplicationBuilder appBuilder)
        {
            using (var serviceScope = appBuilder.ApplicationServices.CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetService<AppDbContext>();
                context.Database.EnsureCreated();

                if (!context.Branches.Any())
                {
                    context.Branches.AddRange(new List<Branch>{
                        new Branch {Name = "Chi nhánh 1", Address = "1 Đinh Tiên Hoàng", City = "HCM", State="Active", ZipCode = "1000" },
                        new Branch {Name = "Chi nhánh 2", Address = "12 Võ Văn Ngân", City = "HCM", State="Active", ZipCode = "2000" },
                        new Branch {Name = "Chi nhánh 3", Address = "20 Võ Thị Sáu", City = "Huế", State="DeActive", ZipCode = "3000"}
                    });
                    context.SaveChanges();
                }
            }
        }
    }
}
