﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FA_ASM2.Data;
using FA_ASM2.Models;
using FA_ASM2.Data.Services;

namespace FA_ASM2.Controllers
{
    public class BranchesController : Controller
    {
        private readonly IBranchesService _branchesService;

        public BranchesController(IBranchesService branchesService)
        {
            _branchesService = branchesService;
        }

        // GET: Branches
        public IActionResult Index()
        {
              return _branchesService.GetAll() != null ? 
                          View(_branchesService.GetAll()) :
                          Problem("Entity set 'AppDbContext.Branches'  is null.");
        }

        // GET: Branches/Details/5
        public IActionResult Details(int id)
        {
            if (_branchesService.GetAll() == null)
            {
                return NotFound();
            }

            var branch = _branchesService.Get(id);
            if (branch == null)
            {
                return NotFound();
            }

            return View(branch);
        }

        // GET: Branches/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Branches/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create([Bind("BranchId,Name,Address,City,State,ZipCode")] Branch branch)
        {
            if (ModelState.IsValid)
            {
                if(branch.State.Equals("active", StringComparison.OrdinalIgnoreCase))
                {
                    branch.State = "Active";
                }
                else if(branch.State.Equals("deactive", StringComparison.OrdinalIgnoreCase))
                {
                    branch.State = "DeActive";
                }
                _branchesService.Add(branch);
                return RedirectToAction(nameof(Index));
            }
            return View(branch);
        }
        // GET: Branches/Edit/5
        public IActionResult Edit(int id)
        {
            if (_branchesService == null)
            {
                return NotFound();
            }

            var branch = _branchesService.Get(id);
            if (branch == null)
            {
                return NotFound();
            }
            return View(branch);
        }

        // POST: Branches/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, [Bind("BranchId,Name,Address,City,State,ZipCode")] Branch branch)
        {
            if (id != branch.BranchId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _branchesService.Update(branch);
                }
                catch (DbUpdateConcurrencyException)
                {
                    return NotFound();
                }
                return RedirectToAction(nameof(Index));
            }
            return View(branch);
        }

        // GET: Branches/Delete/5
        public IActionResult Delete(int id)
        {
            if (_branchesService == null)
            {
                return NotFound();
            }

            var branch = _branchesService.Get(id);
            if (branch == null)
            {
                return NotFound();
            }

            return View(branch);
        }

        // POST: Branches/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            if (_branchesService.GetAll() == null)
            {
                return Problem("Entity set 'AppDbContext.Branches'  is null.");
            }
            var branch = _branchesService.Get(id);
            if (branch != null)
            {
                _branchesService.Delete(id);
            }
            
            return RedirectToAction(nameof(Index));
        }

        

    }
}
